<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('studentProfile','student\StudentProfileController');
Route::resource('stuEditProfile','student\StudentEditProfileController');
Route::resource('sendBookReq','student\SendBookReqController');
Route::resource('bookedCourses','student\BookedCoursesController');
Route::resource('paymentHistory','student\PaymentHistoryController');
Route::resource('subjectDetail','student\SubjectDetailController');
Route::resource('sessionDetail','student\SessionDetailController');
Route::resource('sessionDetail2','student\SessionDetail2Controller');
Route::resource('stuAccountSetting','student\accountSettingController');

Route::resource('tutoreProfile','tutor\tutorProfileController');
Route::resource('tutorAccountSetting','tutor\AccountSettingController');
Route::resource('uploadCourse','tutor\UploadCourseController');