<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccountDetail extends Model
{
	use SoftDeletes;
    protected $table = 'account_detail';
	protected $primaryKey = 'id';
	protected $guarded = ['id'];    
}
