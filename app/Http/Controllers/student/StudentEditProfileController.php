<?php

namespace App\Http\Controllers\student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Auth;

class StudentEditProfileController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['data'] = User::where('id',Auth::user()->id)->first();
        return view('student.editProfile')->with($data);
    }

    public function update(Request $request, $id)
    {
        $param = $request->all();
        unset($param['_token'],$param['_method']);
        $update = User::where('id',$id)->update($param);
        if($update)
        {
            return response()->json(['status' => 'success']);
        }
        else
        {
            return response()->json(['status' => 'error']);
        }
    }
}
