<?php

namespace App\Http\Controllers\student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SendBookReqController extends Controller
{
	/**
	 * Handle the incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function __invoke(Request $request)
	{
		$this->middleware('auth');
	}

	public function index()
	{
		$data['data'] = "";
		return view('student.sendBookReq')->with($data);
	}
}
