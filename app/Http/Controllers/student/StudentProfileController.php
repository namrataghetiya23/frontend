<?php

namespace App\Http\Controllers\student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Auth;

class StudentProfileController extends Controller
{
	/**
	 * Handle the incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function __invoke(Request $request)
	{
		$this->middleware('auth');
	}

	public function index()
	{
		$data['data'] = User::where('id',Auth::user()->id)->first();
		return view('student.profile')->with($data);
	}

}
