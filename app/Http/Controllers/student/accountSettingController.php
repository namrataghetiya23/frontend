<?php

namespace App\Http\Controllers\student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\AccountDetail;
use Auth;

class AccountSettingController extends Controller
{
	/**
	 * Handle the incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function __invoke(Request $request)
	{
		$this->middleware('auth');
	}

	public function index()
	{
		$data['data'] = AccountDetail::where('user_id',Auth::user()->id)->first();
		return view('student.accountSetting')->with($data);
	}

	public function update(Request $request, $id)
	{
		$param = $request->all();
		unset($param['_token'],$param['_method']);
		$update = AccountDetail::where('id',$id)->update($param);
		if($update)
		{
			return response()->json(['status' => 'success']);
		}
		else
		{
			return response()->json(['status' => 'error']);
		}
	}
}
