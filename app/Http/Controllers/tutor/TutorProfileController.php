<?php

namespace App\Http\Controllers\tutor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Auth;

class TutorProfileController extends Controller
{
	/**
	 * Handle the incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function __invoke(Request $request)
	{
		$this->middleware('auth');
	}
	
	public function index()
	{
		$data['data'] = User::where('id',Auth::user()->id)->first();
		return view('tutor.profile')->with($data);
	}
}
