<?php

namespace App\Http\Controllers\tutor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class UploadCourseController extends Controller
{
    /**
	 * Handle the incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function __invoke(Request $request)
	{
		$this->middleware('auth');
	}
	
	public function index()
	{
		$data['data'] = "";
		return view('tutor.uploadCourse')->with($data);
	}
}
