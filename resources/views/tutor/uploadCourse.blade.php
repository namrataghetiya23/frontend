@extends('main')
@section('content')
<section class="contain_wapper" id="contain_wapper">
   <div class="home_wapper">
      <div class="inner-banner">
         <div class="container">
            <div class="banner-content">
               <h1>Upload New Course</h1>
            </div>
         </div>
      </div>
      <div class="container">
      <div class="account-settings">
         <form>
            <h2>Upload New Course</h2>
             <div class="form-group">
                <label for="EnterCourse">Course Title*</label>
                <input type="text" class="form-control" id="EnterCourse" placeholder="Enter Course Title">
              </div>
              <div class="form-group">
                <label for="SubTitle">Course SubTitle</label>
                <input type="text" class="form-control" id="SubTitle" placeholder="Enter Course SubTitle">
              </div>
              <div class="form-group">
                <label for="Category">Course Category*</label>
                <select id="Category" class="form-control">
			        <option selected>Select Course Category</option>
			        <option selected>Select Course</option>
			        <option selected>Select Category</option>
			      </select>
              </div>
              <div class="form-group">
                <label for="Category">Course Subcategory*</label>
                <select id="Category" class="form-control">
			        <option selected>Select Course Subcategory</option>
			        <option selected>Select Course</option>
			        <option selected>Select Subcategory</option>
			      </select>
              </div>
              <div class="form-group">
                <label for="exampleFormControlTextarea1">Course Description *</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" placeholder="Enter Subject Description" rows="3"></textarea>
              </div>
              <div class="form-group">
              	<button class="small-btn form-btn">Add Batch</button>
              </div>
              <div class="form-check form-check-inline form-group">
              	<label class="form-check-label" for="recursive">It is recursive</label>
              	<div class="check-box">
				<input class="form-check-input ml-1" type="checkbox" id="recursive" value="option1">				  
			   	<label></label>
			   </div>
			   </div>
              <div class="form-group row">
                <div class="form-group col-md-6">
                	<label class="form-check-label" for="fromdate">From Date *</label>
                  	<div id="datepicker" class="input-group date" data-date-format="mm-dd-yyyy">
					    <input class="form-control" type="text" readonly />
					    <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
					</div>
                </div>
               <div class="form-group col-md-6">
                	<label class="form-check-label" for="fromdate">To Date *</label>
                  	<div id="datepicker2" class="input-group date" data-date-format="mm-dd-yyyy">
					    <input class="form-control" type="text" readonly />
					    <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
					</div>
                </div>
              </div>
              <div class="form-group">
                <label for="Attendees">No. of Attendees *</label>
                <input type="text" class="form-control" id="Attendees" placeholder="Enter No. of Attendees">
              </div>
              <div class="form-group">
                	<label class="form-check-label" for="fromdate">Select Days *</label>
                  	<div id="datepicker3" class="input-group date" data-date-format="dd-mm-yyyy">
					    <input class="form-control" type="text" readonly placeholder="Select Days" />
					    <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
					</div>
                </div>
                <div class="form-group">
                <label for="CourseFees">Course Fees *</label>
                <input type="text" class="form-control" id="CourseFees" placeholder="Enter Course Fees">
              </div>
              <div class="form-group">
                <label for="agegroup">Age Group *</label>
                <input type="text" class="form-control" id="agegroup" placeholder="Enter Age Group">
              </div>
               <div class="form-group">
               	<label class="orange-text">Session Details:</label>
               	<label class="orange-text">Batch 1:</label>
               </div>
               <div class="form-group">
                <label for="SelectSession">Session Date and Time *</label>
                <input type="text" class="form-control" id="SelectSession" placeholder="Select Session Date and Time">
              </div>
              <div class="form-group">
                <label for="Session">Session  Type (One to One/Group) *</label>
                <select id="Session" class="form-control">
			        <option selected>Select Session Type</option>
			        <option selected>Select Course</option>
			        <option selected>Select Session</option>
			      </select>
              </div>
              <div class="btn default second-btn opacity-5"><a id="save" href="javascript:void(0)">Add More Session</a></div>
				<div class="form-group">
              	<button class="small-btn form-btn">Add More Batch</button>
              </div>
              <div class="btn default"><a id="save" href="javascript:void(0)">Submit</a></div>
         </form>
      </div>
      </div>
   </div>
</section>

@stop