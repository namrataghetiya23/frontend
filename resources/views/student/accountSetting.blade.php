@extends('main')
@section('content')
<section class="contain_wapper" id="contain_wapper">
	<div class="home_wapper">
		<div class="inner-banner">
			<div class="container">
				<div class="banner-content">
					<h1>Account Settings</h1>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="account-settings">
				<form id="edit__form" method="POST">
					@csrf
					@method('PUT')
					<h2>Change Details</h2>
					<div class="form-group">
						<label for="email_id">Email ID *</label>
						<input type="email" class="form-control" id="email_id" name="email_id" placeholder="Enter Email ID" value="{{ $data->email_id }}">
					</div>
					<div class="form-group">
						<label for="password">Enter Password *</label>
						<input type="password" class="form-control" id="password" name="password" placeholder="Enter Password">
					</div>
					<div class="form-group">
						<label for="payment_method">Payment Method *</label>
						<input type="text" class="form-control" id="payment_method" name="payment_method" placeholder="Payment Gateway ID" value="{{ $data->payment_method }}">
					</div>
					<div class="form-group">
						<label for="card_detail">Credit/Debit Card Details *</label>
						<input type="text" class="form-control" id="card_detail" name="card_detail" placeholder="Credit/Debit Card Number" value="{{ $data->card_detail }}">
					</div>
					<div class="form-group row">
						<div class="form-group col-md-4">
							<input type="text" class="form-control" id="month" name="month" placeholder="Ex. Month" value="{{ $data->month }}">
						</div>
						<div class="form-group col-md-4">
							<input type="text" class="form-control" id="year" name="year" placeholder="Ex. Year" value="{{ $data->year }}">
						</div>
						<div class="form-group col-md-4">
							<input type="text" class="form-control" id="cvv" name="cvv" placeholder="CVV" value="{{ $data->cvv }}">
						</div>
					</div>
					<div class="form-group">
						<label for="notification_prefer">Notification Preferences *</label>
						<input type="text" class="form-control" id="notification_prefer" name="notification_prefer" placeholder="Notification Preferences" value="{{ $data->notification_prefer }}">
					</div>
					<div class="btn default"><a id="save" href="javascript:void(0)">Update</a></div>
				</form>
			</div>
		</div>
	</div>
</section>

<script>
	$("#save").on("click", function (e)
	{
		$.ajax({
			type: "POST",
			url: "{{ route('stuAccountSetting.update', array($data->id)) }}",
			data: new FormData($('#edit__form')[0]),
			processData: false,
			contentType: false,
			success: function (data)
			{
				if (data.status === 'success') 
				{
					toastr.options.timeOut = 3000;
					toastr.options.fadeOut = 3000;
					toastr.options.progressBar = true;
					toastr.options.onHidden = function(){
						window.location = "{{ route('stuAccountSetting.index') }}"
					};
					toastr["success"]("Account Details Updated Successfully", "Success");
				}
				else if(data.status === 'error') 
				{
					toastr.options.timeOut = 3000;
					toastr.options.fadeOut = 3000;
					toastr.options.progressBar = true;
					toastr["error"]("Opps.. Something Went Wrong.!", "Error");
				}
			}
		});
	});
</script>
@stop