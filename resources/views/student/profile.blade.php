@extends('main')
@section('content')

<section class="contain_wapper" id="contain_wapper">
   <div class="home_wapper">
	  <div class="inner-banner">
		 <div class="container">
			<div class="banner-content">
			   <h1>My Profile</h1>
			</div>
		 </div>
	  </div>
	  <div class="user-profile">
		 <div class="container">
			<div class="user-header">
			   <img src="images/user-icon.png">
			</div>
			<div class="user-title">
			   <span>StudentDetails</span>
			</div>
			<div class="user-datalist">
			   <ul>
					<li>
						<strong>First Name:</strong>
						<span>{{$data->name}}</span>
					</li>
					<li>
						<strong>Last Name:</strong>
						<span>{{$data->lname}}</span>
					</li>
					<li>
						<strong>Parents name:</strong>
						<span>{{$data->parents_name}}</span>
					</li>
					<li>
						<strong>Contact No.:</strong>
						<span>{{$data->contact}}</span>
					</li>
					<li>
						<strong>Email ID:</strong>
						<span>{{$data->email}}</span>
					</li>
					<li>
						<strong>School:</strong>
						<span>{{$data->school_name}}</span>
					</li>
					<li>
						<strong>City:</strong>
						<span>{{$data->city}}</span>
					</li>
					<li>
						<strong>State:</strong>
						<span>{{$data->state}}</span>
					</li>
					<li>
						<strong>Country:</strong>
						<span>{{$data->country}}</span>
					</li>
					<li>
						<strong>Show Email Id/Phone Number:</strong>
						<span></span>
					</li>
					<li>
						<strong>Address:</strong>
						<span>{{$data->address}}</span>
					</li>
				</ul>
				<div class="btn default">
				  <a href="{{ route('stuEditProfile.index') }}">EDIT PROFILE</a>
				</div>
			</div>
		 </div>
	  </div>
   </div>
</section>

@stop