@extends('main')
@section('content')
<section class="contain_wapper" id="contain_wapper">
	<div class="home_wapper">
		<div class="inner-banner">
			<div class="container">
				<div class="banner-content">
					<h1>Edit Profile</h1>
				</div>
			</div>
		</div>
		<div class="user-profile edit-profile">
			<div class="container">
				<div class="user-header">
					<i class='bx bxs-edit-alt'></i>
					<img src="images/edit-icon.png">
				</div>
				<div class="user-title">
					<span>Student Details</span>
				</div>
				<div class="user-datalist">
					<form id="edit__form" method="POST">
						@csrf
						@method('PUT')
						<ul>
							<li>
								<span>First Name</span>
								<input type="text" placeholder="First Name" name="name" value="{{$data->name}}">
							</li>
							<li>
								<span>Last Name</span>
								<input type="text" placeholder="Last Name" name="lname" value="{{$data->lname}}">
							</li>
							<li>
								<span>Parents name</span>
								<input type="text" placeholder="Parents Name" name="parents_name" value="{{$data->parents_name}}">
							</li>
							<li>
								<span>Contact No.</span>
								<input type="text" placeholder="Contact No." name="contact" value="{{$data->contact}}">
							</li>
							<li>
								<span>Email ID</span>
								<input type="text" placeholder="Email Id" name="email" value="{{$data->email}}">
							</li>
							<li>
								<span>School Name</span>
								<input type="text" placeholder="School Name" name="school_name" value="{{$data->school_name}}">
							</li>
							<li>
								<span>Country Name</span>
								<select>
									<option>Select Country</option>
									<option>india</option>
									<option>Afghanistan</option>
									<option>Indonesia</option>
									<option>Iran</option>
									<option>Japan</option>
								</select>
							</li>
							<li>
								<span>City Name</span>
								<select>
									<option>Select Country</option>
									<option>Ahmedabad</option>
									<option>Surat</option>
									<option>Vadodara</option>
									<option>Rajkot</option>
									<option>Bhavnagar</option>
								</select>
							</li>
							<li>
								<span>State Name</span>
								<select>
									<option>Select State</option>
									<option>Andhra Pradesh</option>
									<option>Haryana</option>
									<option>Assam</option>
									<option>Gujarat</option>
									<option>Goa</option>
								</select>
							</li>

							<li>
								<span>Address</span>
								<textarea name="address">{{$data->address}}</textarea>
							</li>
						</ul>

						<div class="btn default"><a id="save" href="javascript:void(0)"> UPDATE PROFILE </a></div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<script>
	$("#save").on("click", function (e)
	{
		$.ajax({
			type: "POST",
			url: "{{ route('stuEditProfile.update', array($data->id)) }}",
			data: new FormData($('#edit__form')[0]),
			processData: false,
			contentType: false,
			success: function (data)
			{
				if (data.status === 'success') 
				{
					toastr.options.timeOut = 3000;
					toastr.options.fadeOut = 3000;
					toastr.options.progressBar = true;
					toastr.options.onHidden = function(){
						window.location = "{{ route('studentProfile.index') }}"
					};
					toastr["success"]("User Updated Successfully", "Success");
				}
				else if(data.status === 'error') 
				{
					toastr.options.timeOut = 3000;
					toastr.options.fadeOut = 3000;
					toastr.options.progressBar = true;
					toastr["error"]("Opps.. Something Went Wrong.!", "Error");
				}
			}
		});
	});
</script>
@stop