<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link
	href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
	rel="stylesheet">
	<link href="css/bootstrap.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<link href="fonts/fonts.css" rel="stylesheet">
	<link href="fonts/font-awesome.min.css" rel="stylesheet">
	<link href="css/animate.min.css" rel="stylesheet">
	<link href='https://unpkg.com/boxicons@2.0.9/css/boxicons.min.css' rel='stylesheet'>
	<link href="css/style.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
	<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.2/js/toastr.min.js">
	</script>
	<script type="text/javascript">
		if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
			var msViewportStyle = document.createElement('style')
			msViewportStyle.appendChild(
				document.createTextNode(
					'@-ms-viewport{width:auto!important}'
					)
				)
			document.querySelector('head').appendChild(msViewportStyle)
		}
	</script>
</head>
<body>
	<div class="main_wapper">
		<header id="header">
			<div class="header_wapper">
				<div class="container">
					<nav class="navbar navbar-expand-lg">
						<div class="logo"><a href="#"><img src="images/logo.png" alt=""></a></div>
						<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon"></span>
						</button>
						<div class="w-100  d-sm-block  d-md-block d-lg-none"></div>
						<div class="collapse navbar-collapse" id="navbarSupportedContent">
							<ul class="navbar-nav ml-auto">
								<li class="nav-item"><a href="#">About</a></li>
								<li class="nav-item"><a href="#">Category</a></li>
								<li class="nav-item"><a href="#">Blog</a></li>
								
								<li class="nav-item">
									<div class="dropdown">
										@guest
										@if (Route::has('register'))
										<a href="javascript:void(0)" class="btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Get Started</a>
										@endif
										@else
										<a href="javascript:void(0)" class="btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}</a>
										@endguest

										@guest
										@if (Route::has('register'))
										<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
											@endif
											@else
											@if(Auth::user()->role_id == 1)
											<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
												@else
												<div class="dropdown-menu white-dropdown" aria-labelledby="dropdownMenuButton">
													@endif
													@endguest

													@guest
													<a class="dropdown-item" href="{{ route('login') }}">Log In</a>
													@if (Route::has('register'))
													<a class="dropdown-item" href="{{ route('register') }}">Register</a>
													@endif
													@else

													@if(Auth::user()->role_id == 1) <!-- 1 For Studemt -->
													<a class="dropdown-item" href="#">My History</a>
													<a class="dropdown-item" href="{{route('studentProfile.index')}}">My Profile</a>
													<a class="dropdown-item" href="{{route('sendBookReq.index')}}">My Send Booking Request</a>
													<a class="dropdown-item" href="{{route('bookedCourses.index')}}">My Booked Courses</a>
													<a class="dropdown-item" href="#">My Favorite Tutors</a>
													<a class="dropdown-item" href="#">My Given Reviews</a>
													<a class="dropdown-item" href="{{route('paymentHistory.index')}}">Payment History</a>
													<a class="dropdown-item" href="#">My Wallet</a>
													<a class="dropdown-item" href="{{ route('stuAccountSetting.index') }}">Account Setting</a>
													<a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
													<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
														@csrf
													</form>
													@else
													<a class="dropdown-item" href="{{route('tutoreProfile.index')}}">My Profile</a>
													<a class="dropdown-item" href="#">My Received Booking Request</a>
													<a class="dropdown-item" href="{{route('uploadCourse.index')}}">My Uploaded Course</a>
													<a class="dropdown-item" href="#">My Booked Session</a>
													<a class="dropdown-item" href="#">Availability Calendar</a>
													<a class="dropdown-item" href="#">Upload New Course</a>
													<a class="dropdown-item" href="#">My Receieved Reviews</a>
													<a class="dropdown-item" href="#">My Wallet</a>
													<a class="dropdown-item" href="#">Payment History</a>
													<a class="dropdown-item" href="{{ route('tutorAccountSetting.index') }}">Account Setting</a>
													<a class="dropdown-item" href="#">Search</a>
													<a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
													<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
														@csrf
													</form>
													@endif
													@endguest
												</div>

											</div>
										</li>

								<!-- <li class="nav-item">
									<div class="dropdown">
										<a href="javascript:void(0)" class="btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Get Started</a>
										<div class="dropdown-menu white-dropdown" aria-labelledby="dropdownMenuButton">
											<a class="dropdown-item" href="#">Action</a>
											<a class="dropdown-item" href="#">Another action</a>
											<a class="dropdown-item" href="#">Something else here</a>
										</div>

									</div>
								</li> -->

							</ul>
						</div>
					</nav>
				</div>
			</div>
		</header>