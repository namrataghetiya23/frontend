@extends('main')
@section('content')

<section class="contain_wapper login-bg" id="contain_wapper">
	<div class="home_wapper">
		<div class="container">
			<form method="POST" action="{{ route('register') }}">
				@csrf
				<div class="title">
					<h2>Register</h2>
				</div>
				<div class="form-group">
					<select class="form-control @error('role_id') is-invalid @enderror" id="role_id" name="role_id" autofocus onchange="roleChange()">
						<option value="">Select User type *</option>
						<option value="1">Student </option>
						<option value="2">Tutor</option>
					</select>
					@error('role_id')
					<span class="invalid-feedback" role="alert">
						<strong>{{ $message }}</strong>
					</span>
					@enderror
				</div>
				<div class="form-group">
					<input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" placeholder="Enter First Name *">

					@error('name')
					<span class="invalid-feedback" role="alert">
						<strong>{{ $message }}</strong>
					</span>
					@enderror
				</div>
				<div class="form-group">
					<input id="lname" type="text" class="form-control @error('lname') is-invalid @enderror" name="lname" value="{{ old('lname') }}" required autocomplete="lname" placeholder="Enter Last Name *">

					@error('lname')
					<span class="invalid-feedback" role="alert">
						<strong>{{ $message }}</strong>
					</span>
					@enderror
				</div>
				<div class="form-group">
					<input id="contact" type="text" class="form-control @error('contact') is-invalid @enderror" name="contact" value="{{ old('contact') }}" required autocomplete="contact" placeholder="Enter Mobile Number *">

					@error('contact')
					<span class="invalid-feedback" role="alert">
						<strong>{{ $message }}</strong>
					</span>
					@enderror
				</div>
				<div class="form-group">
					<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Enter Email Id *">

					@error('email')
					<span class="invalid-feedback" role="alert">
						<strong>{{ $message }}</strong>
					</span>
					@enderror
				</div>
				<div class="form-group">
					<input id="user_name" type="text" class="form-control @error('user_name') is-invalid @enderror" name="user_name" value="{{ old('user_name') }}" required autocomplete="user_name" placeholder="Enter Username *">

					@error('user_name')
					<span class="invalid-feedback" role="alert">
						<strong>{{ $message }}</strong>
					</span>
					@enderror
				</div>
				<div class="form-group">
					<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Enter Password *">
					@error('password')
					<span class="invalid-feedback" role="alert">
						<strong>{{ $message }}</strong>
					</span>
					@enderror
				</div>
				<div class="form-group">
					<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Enter Confirm Password *">
				</div>
				<div class="form-group">
					<input id="school_name" type="text" class="form-control @error('school_name') is-invalid @enderror" name="school_name" value="{{ old('school_name') }}" required autocomplete="school_name" placeholder="Enter Confirm Password *">

					@error('school_name')
					<span class="invalid-feedback" role="alert">
						<strong>{{ $message }}</strong>
					</span>
					@enderror
				</div>
				<div class="form-group">                    
					<div class="file-upload-wrapper" data-text="Select your file!">
						<input name="document" type="file" class="file-upload-field" value="">
					</div>
				</div>
				<div class="form-group">
					<select class="form-control @error('country') is-invalid @enderror" id="country" name="country" >
						<option value="">Select Country *</option>
						<option value="1">Country 1 </option>
						<option value="2">Country 2</option>
						<option value="3">Country 3</option>
						<option value="4">Country 4</option>
						<option value="5">Country 5</option>
					</select>
					@error('country')
					<span class="invalid-feedback" role="alert">
						<strong>{{ $message }}</strong>
					</span>
					@enderror
				</div>
				<div class="form-group">
					<select class="form-control @error('state') is-invalid @enderror" id="state" name="state" >
						<option value="">Select State *</option>
						<option value="1">State 1 </option>
						<option value="2">State 2</option>
						<option value="3">State 3</option>
						<option value="4">State 4</option>
						<option value="5">State 5</option>
					</select>
					@error('state')
					<span class="invalid-feedback" role="alert">
						<strong>{{ $message }}</strong>
					</span>
					@enderror
				</div>
				<div class="form-group">
					<select class="form-control @error('city') is-invalid @enderror" id="city" name="city" >
						<option value="">Select City *</option>
						<option value="1">City 1 </option>
						<option value="2">City 2</option>
						<option value="3">City 3</option>
						<option value="4">City 4</option>
						<option value="5">City 5</option>
					</select>
					@error('city')
					<span class="invalid-feedback" role="alert">
						<strong>{{ $message }}</strong>
					</span>
					@enderror
				</div>
				<button type="submit" class="btn btn-primary">REGISTER NOW</button>
				<div class="signup-link">
					<span>You already have an accoun? <a href="{{ route('login') }}" class="orange-text">Log in here</a></span>
				</div>
				<div class="divider">
					<span></span>
					<label>OR</label>
					<span></span>
				</div>
				<div class="singup-opction">
					<button class="btn google-btn"><i class="fa fa-google" aria-hidden="true"></i> Sign in With Google</button>
					<button class="btn fb-btn"><i class="fa fa-facebook-official" aria-hidden="true"></i> Sign in With Facebook</button>
					<button class="btn linkedin-btn"><i class="fa fa-linkedin-square" aria-hidden="true"></i> Sign in With Linkedin</button>
				</div>
			</form>
		</div>
	</div>
</section>

<script>
	$("#country").hide();
	$("#state").hide();
	$("#city").hide();

	function roleChange(){
		role_id = $("#role_id").val();
		if(role_id == 1 || role_id == ""){
			$("#country").hide();
			$("#state").hide();
			$("#city").hide();
		}
		else{
			$("#country").show();
			$("#state").show();
			$("#city").show();
		}
	}
</script>
@endsection
