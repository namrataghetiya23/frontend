@extends('main')
@section('content')
<section class="contain_wapper login-bg" id="contain_wapper">
    <div class="home_wapper">
        <div class="container">
            <form method="POST" action="{{ route('login') }}">
                <div class="title">
                    <h2>Log in</h2>
                </div>
                
                @csrf
                <div class="form-group">
                    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus id="email" placeholder="Enter Email ID / Mobile Number *">
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" required id="password" placeholder="Enter Password *">
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="gridCheck">
                            <label class="form-check-label" for="gridCheck">
                                Remember Me
                            </label>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="form-check-label" for="gridCheck">
                            Forgot password ?
                        </label>
                    </div>
                </div>
                
                <button type="submit" class="btn btn-primary">Sign in</button>
                <div class="signup-link">
                    <span> New to our site? <a href="{{ route('register') }}" class="orange-text">Sign Up here</a></span>
                </div>
                <div class="divider">
                    <span></span>
                    <label>OR</label>
                    <span></span>
                </div>
                <div class="singup-opction">
                    <button class="btn google-btn"><i class="fa fa-google" aria-hidden="true"></i> Sign in With Google</button>
                    <button class="btn fb-btn"><i class="fa fa-facebook-official" aria-hidden="true"></i> Sign in With Facebook</button>
                    <button class="btn linkedin-btn"><i class="fa fa-linkedin-square" aria-hidden="true"></i> Sign in With Linkedin</button>
                </div>
            </form>
        </div>
    </div>
</section>
@stop